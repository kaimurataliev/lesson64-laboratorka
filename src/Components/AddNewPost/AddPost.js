import React, { Component, Fragment } from 'react';
import Header from '../../UI/Header/Header';
import Form from '../../UI/Form/Form';
import './AddPost.css';
import axios from '../../axios';

export default class AddPost extends Component {

    state = {
        title: '',
        body: ''
    };

    getTitle = (event) => {
        this.setState({title: event.target.value});
    };

    getBody = (event) => {
        this.setState({body: event.target.value});
    };

    savePost = (e) => {
        e.preventDefault();

        const post = this.state;
        post.createdAt = new Date();
        post.editedAt = new Date();
        axios.post('/posts.json', post)
            .then(() => {
                this.setState({
                    title: '',
                    body: ''
                })
            })
    };

    render() {
        return (
            <div className="add">
                <Header locate="Add"/>
                <div className="add-new-post">
                    <Form savePost={this.savePost}
                          getBody={this.getBody}
                          getTitle={this.getTitle}
                          titleValue={this.state.title}
                          bodyValue={this.state.body}
                    />
                </div>
            </div>
        )
    }
}