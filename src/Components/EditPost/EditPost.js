import React, { Component, Fragment } from 'react';
import Header from '../../UI/Header/Header';
import axios from '../../axios';
import './EditPost.css';

export default class EditPost extends Component {

    state = {
        title: '',
        body: '',
        createdAt: '',
        editedAt: ''
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`/posts/${id}.json`)
            .then(response => {
                this.setState(response.data);
            })
    }

    getTitle = (event) => {
        this.setState({title: event.target.value});
    };

    getBody = (event) => {
        this.setState({body: event.target.value});
    };

    editPost(id, event) {
        event.preventDefault();
        axios.put(`/posts/${id}.json`, this.state).finally(() => {
        });
    }

    render() {
        const id = this.props.match.params.id;
        return (
            <div className="edit-page">
                <Header/>
                <form className="edit-form">
                    <h1>Edit post</h1>
                    <input onChange={this.getTitle} type="text" value={this.state.title}/>
                    <textarea onChange={this.getBody} name="body" cols="30" rows="10" value={this.state.body}/>
                    <button onClick={(event) => this.editPost(id, event)}>Edit Post</button>
                </form>
            </div>
        )
    }
}