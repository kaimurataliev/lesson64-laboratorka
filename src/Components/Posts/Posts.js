import React from 'react';
import { NavLink } from 'react-router-dom';

const Posts = props => {
    const posts = Object.keys(props.posts);

    return (
        <div>
            {posts && posts.map((id, index) => {
                const date = new Date(props.posts[id].editedAt);
                const link = `/posts/${id}`;
                return (
                    <div key={index} className="post-list">
                        <h4>{props.posts[id].title}</h4>
                        <p>{date.toDateString()}</p>
                        <NavLink className='read-more' to={link}>Read More</NavLink>
                    </div>
                )
            })}
        </div>
    )
};

export default Posts;