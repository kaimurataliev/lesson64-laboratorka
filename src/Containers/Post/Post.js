import React, { Component, Fragment } from 'react';
import axios from '../../axios';
import './Post.css';
import Header from '../../UI/Header/Header';
import { NavLink, Redirect } from 'react-router-dom';

export default class Post extends Component {

    state = {
        title: '',
        body: '',
        createdAt: '',
        editedAt: '',
        redirect: false
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`/posts/${id}.json`)
            .then(response => {
                this.setState(response.data);
            })
    }

    deletePost(key) {
        axios.delete(`/posts/${key}.json`).finally(() => {
            this.setState({redirect: true})
        });
    }

    render() {
        const date = new Date(this.state.editedAt);
        const id = this.props.match.params.id;
        const link = `/posts/${id}/edit`;
        return (
            <div>
                <Header/>
                <div className="post">
                    <h4>{this.state.title}</h4>
                    <p>{this.state.body}</p>
                    <span>Date: {date.toDateString()}</span>
                    <button onClick={() => this.deletePost(id)}>Delete Post</button>
                    {this.state.redirect && <Redirect to="/"/>}
                    <NavLink className='edit-post' to={link}>Edit Post</NavLink>
                </div>
            </div>
        )
    }
}