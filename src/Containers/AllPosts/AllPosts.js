import React, { Component, Fragment } from 'react';
import Header from '../../UI/Header/Header';
import './AllPosts.css';
import axios from '../../axios';
import Posts from '../../Components/Posts/Posts';

export default class AllPosts extends Component {

    state = {
        posts: {}
    };

    getPosts() {
        axios.get('/posts.json')
            .then(response => {
                this.setState({posts: response.data ? response.data : {}})
            })
    }

    componentDidMount() {
        this.getPosts();
    }


    render() {
        return (
            <Fragment>
                <Header locate="All"/>
                <div className="all-posts">
                    <Posts posts={this.state.posts}/>
                </div>
            </Fragment>
        )
    }
}