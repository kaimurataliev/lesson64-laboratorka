import React, { Component, Fragment } from 'react';
import { Route, NavLink, Switch } from 'react-router-dom';
import AllPosts from './Containers/AllPosts/AllPosts';
import AddPost from './Components/AddNewPost/AddPost';
import EditPost from './Components/EditPost/EditPost';
import Post from './Containers/Post/Post';
import About from './Components/About/About';


export default class App extends Component {
    render() {
        return (
            <Fragment>
                <Switch>
                    <Route path="/" exact component={AllPosts}/>
                    <Route path="/posts/add" component={AddPost} />
                    <Route path="/posts/about" component={About}/>
                    <Route path="/posts/:id/edit" component={EditPost}/>
                    <Route path="/posts/:id" component={Post}/>
                </Switch>
            </Fragment>
        )
    }
}