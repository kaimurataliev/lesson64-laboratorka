import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';

const Header = (props) => {
    return (
        <header className="header-fixed">
            <div className="header-limiter">

                <h1>Posts</h1>

                <nav>
                    <NavLink to="/" className={props.locate === 'All' ? 'selected' : ''}>All Posts</NavLink>
                    <NavLink to='/posts/add' className={props.locate === 'Add' ? 'selected' : ''}>Add new post</NavLink>
                    <NavLink to="/posts/about" className={props.locate === 'About' ? 'selected' : ''}>About</NavLink>
                </nav>
            </div>
        </header>
    )
};

export default Header;