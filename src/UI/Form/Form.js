import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <form className="form">
            <h1>Add new post</h1>
            <input onChange={props.getTitle} value={props.titleValue} type="text" name="title" placeholder="Title"/>
            <textarea onChange={props.getBody} value={props.bodyValue} name="body" cols="30" rows="10"/>
            <button onClick={props.savePost}>save</button>
        </form>
    )
};

export default Form;